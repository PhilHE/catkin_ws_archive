// 串口订阅读取信息,订阅主题自listener
/* 以下为读取信息 FL.FR,RL,RR(前/后 左/右)
57字节
motor_position[4] int
motor_speed[4] int
motor_current[4] int 
motor_temperature[4] char
theta/omiga/vx/vy/vz float
ax/ay/az int
flag:3bytes 用于校正
*/
// 串口发布写入信息，发布主题自talker
// uint8_t TxData[8];
/****************************************************************************/
//ros需要的头文件
#include "ros/ros.h"
#include <std_msgs/String.h>
#include <std_msgs/Empty.h>

//以下为串口通讯需要的头文件
#include <string>        
#include <iostream>
#include <cstdio>
#include <unistd.h>
#include <math.h>
#include "serial/serial.h"
/****************************************************************************/
using std::string;
using std::exception;
using std::cout;
using std::cerr;
using std::endl;
using std::vector;
/*****************************************************************************/
// 各数据定义
int motor_position[4];
int motor_speed[4];
int motor_current[4];
char motor_temperature[4];
float theta, omiga, vx, vy, vz; 
int ax, ay, az;
// 识别位
unsigned char data_terminal[3];
data_terminal[0] = 0xA1;
data_terminal[1] = 0xA2;
data_terminal[2] = 0xA3;
// 创建判定字符数组 data_ok[3]
char data_ok[3];
/*****************************************************************************/
serial::Serial ros_ser;

// 回调函数
void callback(const std_msgs::String::ConstPtr& msg){
     ROS_INFO_STREAM("Write to serial port" << msg->data);
     ros_ser.write(msg->data);
 }

// 主函数
int main(int argc, char **argv)
{
    ros::init(argc, argv, "serial_rw");//初始化串口节点
    ros::NodeHandle n;  //定义节点进程句柄

    ros::Subscriber sub = n.subscribe("listener", 20, callback); //订阅自/listener主题
    //定义要发布/talker主题，超过20个舍弃
    ros::Publisher talker_pub= n.advertise<nav_msgs::Odometry>("talker", 20);       

    try
    {
        ros_ser.setPort("/dev/ttyUSB0");//小车串口号
        ros_ser.setBaudrate(115200);//小车串口波特率
        ros_serial::Timeout to = serial::Timeout::simpleTimeout(1000);
        ros_ser.setTimeout(to);
        ros_ser.open();//配置串口
    }
    catch (serial::IOException& e)
    {
        ROS_ERROR_STREAM("Unable to open port ");
        return -1;
    }
    if(ros_ser.isOpen()){
        ROS_INFO_STREAM("Serial Port opened");
    }
    else{
        return -1;
    }
    // 循环周期是100Hz，设置周期休眠时间
    ros::Rate loop_rate(100);
    while(ros::ok())
    {
        rec_buffer = ros_ser.readline(57,"\n");    //获取串口发送来的数据
        const char *receive_data = rec_buffer.data(); //保存串口发送来的数据
        // 数据与处理与判定

        if(rec_buffer.length()==57 || data_ok[3] == data_terminal[3]) //串口接收的数据长度正确就处理
        {
            // 开始进行数据处理
            
            ros::spinOnce();//周期执行
            loop_rate.sleep();//周期休眠
        }
        //程序周期性调用
        //ros::spinOnce();  //callback函数必须处理所有问题时，才可以用到
    }
    return 0;
}