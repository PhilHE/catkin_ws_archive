/*********************************
 * 串口节点，订阅cmd_vel话题并发布odometry话题
 * 从cmd_vel话题中分解出速度值通过串口送到移动底盘
 * 从USB接收里程消息整合到odometry话题用于发布
 * 
 * *******************************/
#include <ros/ros.h>
#include <serial/serial.h>
#include <msg/base_rw.h>

// #define	sBUFFERSIZE	15//send buffer size 串口发送缓存长度
// #define	rBUFFERSIZE	27//receive buffer size 串口接收缓存长度
// unsigned char s_buffer[sBUFFERSIZE];//发送缓存
// unsigned char r_buffer[rBUFFERSIZE];//接收缓存

/************************************
 * 串口数据发送格式共x字节(原有框架)
 * head head linear_v_x  linear_v_y angular_v  CRC
 * 0xff 0xff float       float      float      u8
 * 
/**********************************************************
 * 串口接收数据格式共x字节(原有框架)
 * head head x-position y-position x-speed y-speed angular-speed pose-angular CRC
 * 0xaa 0xaa float      float      float   float   float         float(yaw)   u8
 * ********************************************************/

//订阅turtle1/cmd_vel话题的回调函数，用于显示速度以及角速度
void readfrom_callback(const geometry_msgs::Twist& cmd_vel){
	
	data_pack(cmd_vel);
}
int main (int argc, char** argv){
    ros::init(argc, argv, "my_serial_node");
    ros::NodeHandle nh;

	//订阅/readfrom话题用于测试
	ros::Subscriber write_sub = nh.subscribe("/readfrom",1000,readfrom_callback);


    //相关串口数据确定校验
    try
    {
        //端口
        ser.setPort("/dev/ttyUSB0");
        //波特率
        ser.setBaudrate(115200);
        serial::Timeout to = serial::Timeout::simpleTimeout(1000);
        ser.setTimeout(to);
        ser.open();
    }
    catch (serial::IOException& e)
    {
        ROS_ERROR_STREAM("Unable to open port ");
        return -1;
    }

    if(ser.isOpen()){
        ROS_INFO_STREAM("Serial Port initialized");
    }else{
        return -1;
    }
    //100hz频率执行
    ros::Rate loop_rate(100);
    while(ros::ok()){

        ros::spinOnce();

// to be continued

        }
        loop_rate.sleep();

    }
}